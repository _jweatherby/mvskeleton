MVSkeleton Installation + Usage
===============================

Index
-----

* [Introduction](#markdown-header-introduction)
* [Installation](#markdown-header-installation)
* [Configuration](#markdown-header-configuration)
* [Routing](#markdown-header-routing)
* [Request](#markdown-header-request)
* [Controllers / Roles / Admin](#markdown-header-controllers-roles-admin)
* [Models](#markdown-header-models)
* [Error Handling](#markdown-header-error-handling)


##Introduction

This is a mid-weight framework created while researching other frameworks.
The whole of the Internet leans towards three major frameworks:

* Zend
* Yii
* Symfony2

This framework was inspired by all 3. Taking what felt like the best 
features and putting them into an easy to use / setup format. 
Having used this framework for several small projects now, the only
thing missing is documentation, so here goes!

The General Idea
----------------

Most projects involve a main section, a mobile view, different 
routing options, a CMS with authentication and maybe even a CRM. 
There can be different types of apps as well, the two most common
being JSON driven (flatfile / nosql apps) or MySQL driven apps. 

This framework takes the contents of 

	framework/core
	framework/helpers

as the core of the framework including the Router, the Request,
default Errors/Exceptions, additional Utilities and the BaseController. 

From there, when initializing the app, you declare the app type, 
whether it be nosql or mysql, and the section you want to build. If neither
of these are satisfactory, simply create a a new folder under framework/, 
put your engine in there and declare it when you initialize the app.

The section is a big part because there can be wildly different templates, 
authentication, actions, etc that need to be separated from each other, but 
use the same core model. These are easy to create in the steps outlined 
in the below configuration section.

Finally, because of how the framework is set up, multiple apps can be 
initialized using the same core. Duplicate the app/ directory, point
a new virtualhost to it, and voila, a whole new site can be set up. 

* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 


## INSTALLATION


To clone:

	git clone git@bitbucket.org:dukeofweatherby/mvskeleton.git

VirtualHost

	<VirtualHost *:80>
 		ServerName gastech.cms #OR any of your choosing
 		DocumentRoot /path/to/repo/./app/web
 		<Directory /path/to/repo>
 			Options +Indexes +Multiviews +FollowSymLinks 
 			AllowOverride FileInfo Options
 		</Directory>
	</VirtualHost>

**Options +Indexes +Multiviews +FollowSymLinks** *enables htaccess to work*

Install the vendors

	php composer.phar install

Make the following folders editable by apache: 

	sudo chgrp www-data app/_logs app/_tmp
	sudo chmod -R g+w app/_logs
	sudo chmod -R g+w app/_tmp

*Note that www-data may need to be changed if not using apache2*

* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 


##CONFIGURATION

In app/config there is a document laid out: 

	app/config/default.php 

This is where the default environment settings are stored.
It is advised to put the DEV ENVIRONMENT SETTINGS + secure keys 
(DB, Access Tokens, etc.) in this file, then override these settings 
on staging and production environments. 

**Do not put production settings (DB info, Live Acces Tokens etc) 
in default.php as it will be available on git, and therefore not 
secure if anybody gets access. Put them in env.php and keys.php**

To override these settings, create additional files:

	app/config/env.php
	app/config/keys.php

These will be included in gitignore, therefore can be used to 
configure the different environments.

The format for these files is to return an array as shown:
	
	return array(
		var_name => array(settings), 
		var_name => value, 
		etc => etc
	);

Additionally, if you wish to add sections (i.e. MAIN, CMS, CRM, etc)
create new folders in 

	app/web/{section_name}/index.php

*Note the section_name is case sensitive*

In index.php, initialize the app

	require_once(/path/to/repo/./App.php);

	App::init(
		$app_path 	= /path/to/repo/./app, 
		$app_type 	= '{app_type}',				// nosqlapp, mysqlapp, custom
		$section 	= '{section_name}'			// MAIN, CMS, CRM, etc
	);

	App::htmlRequest($_REQUEST);

*Note the app_type is case sensitive*

Then create files within config/ of the same {section_name} in **lower case**:

	MAIN => app/config/main.php
	CMS => app/config/cms.php

These will be included with the section being called and will override settings
set in default.php.

Then place additional settings within, including modified 

	CTRL_PATH
	VIEW_PATH
	MOBILE_PATH

Pointing them to the correct root directories.

	'CTRL_PATH' 	=> "mvc/Controllers/{section_name}"
	'VIEW_PATH' 	=> "mvc/Views/{section_name}"
	'MOBILE_PATH' 	=> "mvc/Views/{section_name}"

Naming Conventions
------------------

There are different naming conventions the classes vs the controllers vs
the views and so on and so forth. 

All files that contain a class are named with a ***.class.php**
The controllers are named likewise, but the class names for controllers
must end in Controller. This is for the sake of clarity and to avoid 
multiple class names.

	TestController extends BaseController

View Layouts
------------

This framework uses a very simple, custom Template engine called Tpl.
It works through the three basic functions:
	
To initialize Tpl, in the BaseController, the following function is
called: 

	Tpl::init($view_template);

This retrieves the name of the controller and action from the base view
directory.

	Test/home > Tpl::init("Test/home.php");

This is automatically returned from 

	$this->getView();

But can be overridden with

	$this->getView($payload, "Test/some_other_template");

When using the templates, there are two different primary functions:

	// To retrieve the layout, "begin" and assign $some_var for use by this 
	// 	layout, the following function is used.
	Tpl::getLayout("begin", array('some_var'=>$some_var)); 

	// To retrieve a specific layout from somewhere else, and assign
	// 	$tags for that template's use, the following can be called:
	Tpl::insert("Test/tag_list", array('tags'=>$tags)); 

With all templates called, the following variables are automatically assigned

	$_req 		// The Request Object
	$_params 	// The parameters passed through the request
	$_data		// The payload passed to the request.


* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 


##ROUTING

The routing and configuration was inspired by Yii. It uses the exact 
same format, and a pretty similar syntax convention. Largely based
on regular expressions, it does have key words for increased readability. 

Everything after the '/' is routed onto the variable $_r but only if a 
matching directory or filename is not found. This is then parsed 
by the router using the patterns found in the config file. This is one 
of the options most likely to be overridden in app/config/{section_name}.php.

Data Types
----------

An important note about the router is Data Types. There are two ways of 
triggering a datatype: 

1. Appending to the URL .{data_type}
2. Adding the parameter ?dt={data_type}

The default is html, but the allowed types include json, csv, xml, etc.
For example: 

	/test 				// html (default)
	/test.html 			// html
	/test?dt=html 		// html
	/test.json 			// json
	/test?dt=json 		// json
	/.json 				// json
	/?dt=json 			// json

The allowable types are included in app/config/default.php and modifiable 
as needed. The actual implementation is handled by BaseController.

*actual handling of these datatypes is up to the user in the controller*

The data types stripped before the routing takes place. Also, these will
be ignored if there is a file that already has that exact path/filename.

Router Functionality
--------------------

The router basically translates a regular expression into parts based on
the rules is was given. The default configuration is as follows:

	'ROUTER' => array(

		"([a-zA-Z]*)/?(\d*)/?([a-zA-Z\-_]*)/?" => 
			"{controller:$1}/{id:$2}/{action:$3}"
	)

It takes the expression at the beginning and then assigns it to the 
corresponding parameter. 

	controller/id/action

Whenever a bracketted expression ('([a-z]+', '([0-9]*)') or 
shortcut (':char', ':num', etc) is found, it is treated as a dynamic
variable and represented by the order it is found. The first experssion, 
**"([a-zA-Z]*)"** is shown as **{controller:$1}** because it is the first dynamic
variable. The second, **"(\d*)"** is the second dynamic variable, so is assigned
to id via **{id:$2}**.

The above default router configuration can be broken down as follows.

If nothing is found, it will use the 

	DEFAULT_CONTROLLER

setting in the configuration file.

If **only a controller** is found, it will automatically assume the action
is **home**. 

If **an id is found, but no action**, it will automatically
to go **/controller/view** and search for that view. It can be set up to 
direct to a 404 using 

	if(!$obj->id) throw new Exception({error message}, 404);

*This also applies to slugs, but that will be covered in a bit*

If an action is found, but no id, it will automatically go that 
controller and action. This is through the magic of regular expressions.

Directly translated into a regular expression, here's a breakdown of
what's happening:

1. Controller
	
	([a-zA-Z]*)/? 

	It will match any of the below scenarios

		/
		/test
		/test/

	This is searching any number of characters (including 0 characters). This
	is the controller. The question mark at the end indicates is doesn't matter
	whether the '/' is found or not, but it will be expected to lead into the id
	or the following action.

2. ID

	(\d*)/?

	It will match any of the below scenarios

		/{controller}/1
		/{controller}/1/
		/{controller}/23
		/{controller}/23/

	This is searching for any number of numbers (including 0 numbers). This is the
	id of an object. The default action is the view when an id or a slug is found.

3. Action

	([a-zA-Z\-_]*)

	It will match any of the below scenarios

		/{controller}/testing
		/{controller}/testing/
		/{controller}/{id}/testing
		/{controller}/{id}/testing/

	Because of the way the id is formatted (any including 0 numbers + the /? at 
	the end), the id doesn't actually have to be present for the action to exist.
	This is, again, due to the magic of regular expressions. 

	There are some key words that are allowed instead of straight up regular
	expressions, and they include:

		:num --> ([0-9]+)
		:char --> ([a-zA-Z]+)
		:alpha --> (0-9A-Za-z_\-]+)

	These are available in the defaut.php and are fully changeable as desired.

Common patterns
---------------

One common variation will using a slug instead of an id. The pattern will
look like: 
	
	"test/:alpha" => "{controller:test}/{slug:$1}"

The default action for slugs to go to the view. This is not included in
app/config/default.php because it is entirely dependent on whether the 
controller's object contains a slug variable and whether it can
be accessed by the public.

* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 


##REQUEST

In Symfony2, there are two parts to any request, the Request and the 
Response. This framework deals quite a bit with the singleton
design pattern in that there is only one class, the Request, and 
it accumulates information as until the request completes. 
When the app initializes an htmlRequest, a request is created. 
It gathers the router configurations, general configurations, 
server settings, parameters passed, data type and returns an 
instance of itself as a response. The private information is kept 
private, the public information available for the request to use and 
display as needed. 

In any controller, the information can be passed from the controller
to the view via the request by using: 

	class TestController extends BaseController
	{
		public function home()
		{
			return $this->getView(new Test());
		}
	}

What this snippet of code does is to pass a new 'Test' object
to 
	
	Request::singleton()->payload; 

as handled by the BaseController. 

A typical request will return the folling information:

	request_type 	: whether a post or get request was sent
	routed_request 	: the parsed url ($_REQUEST['_r'])
	controller 		: the returned controller
	action 			: the returned action
	dt 				: the returned data type
	params: 		: the parsed parameters
	duration 		: how long the request took (milliseconds)
	success 		: whether the request was successful
	code 			: the internal code (used by errors)
	msg 			: the resulting message
	payload 		: the data returned from the view
	redir 			: any sort of redirection requested
	ctrl_class 		: the full class of the controller
	peak_usage 		: most memory used during the request
	e 				: the exception, if one was found

The most relevant Request methods are the following:

	getException 	: returns the exception, if one was found
	getViewFormat	: returns the DataType parsed ($_REQUEST['dt'])
	getParams 		: gets all parameters passed, including
						custom ones passed through the router


* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 


##Controllers-Roles-Admin

To create roles for specific pages, this is primarily done in via
the BaseController and the Admin Model. There is a separate folder within the 
app:

	app/admin

This will, by default, contain 3 files:

	app/admin/PublicController.class.php
	app/admin/SecureController.class.php
	app/admin/Admin.class.php

When creating a controller and to add a level of security, simply inherit
one of the following and add the code to validate the controller being
used. For example, in the PublicController, this is probably going to be 
nearly empty, as no authenticated user is required.

	app/admin/PublicController.class.php

The SecureController will contain a constructor that validates the user
before allowing the user to continue. This will throw and 
AuthException and the Error handler will handle that in a special 
manner, probably via login (401), or forbidden message (403).

	app/admin/SecureController.class.php

This simply checks whether a logged in user has been found. 
Additional levels of security can be implemented by creating these 
new controllers that verify based on different levels of authentication. In
addition. These functions will be handled by Admin, a Model that handles this
functionality. The admin is just a default, and is meant to be customized
for the app at hand.


* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 


##MODELS

The models in this framework are handled via a BaseModel, though
there is the flexibility to handle them however one pleases. All the 
classes found in app/mvc/Models will be automatically loaded by the 
autoloader if they have the extension '.class.php'

With the BaseModel provided, the classes are initialized as followed:

	/app/mvc/Models/Test.class.php

	class Test extends BaseModel
	{
		public 
			$id, $title, $description, $tags = array(); 

		protected 
			$_table = 'test',
			$_db_vars = array('id', 'title', 'description');
	}

	class Tags extends BaseModel
	{
		public 
			$id, $name, $slug; 

		protected
			$_table = 'test_tag',
			$_db_vars = array('id', 'name', 'slug');
	}

In this example, test uses the table 'test' and the variables in
the database include the 'id', 'title', and 'description'. Not included
in the database are the tags, because this is a one-to-many relationship. 
The tags are similar in structure, and what will likely be created is 
another table, 'test_tags'. In the 'Test' model, there will be a function
'getTags' or something similar, where the tags will be retrieved and 
placed into the '$tags' variable. The structure is loosely coupled
with the database structure, as opposed to the strict coupling that 
ORMs enforce. 

The below listed functionalities are used by both the 'mysqlapp', and 
'nosqlapp' listed in the configuration. The similar naming convention
was chosen for ease of implementation, though situations will come up
where a completely different way of thinking is required to deal with 
specific situations. 

The different functionalities associated with the BaseModel include the
following (using Test.class.php as an example):


1. Loading
	
	There are several ways to load a model: through its 'id' variable;
	through its 'slug' variable; through an array of variables 
	combined via the 'AND' operator; or, (only in mysqlapp), a 
	static 'where' query that uses the arguments passed directly
	as a string to the select statement. Examples of all 4 follow.

		// Standard loading of an element via id (id is numeric)
		$test = new Test($id = 2); 		// will load a test with id=2

		// The following will only work for slug, and if slug is declared
		//	as a _db_var
		$test = new Test($slug='test'); 

		// an array of information to load from. Uses AND operator
		$test = new Test(array(			// this will load a test With 
			'id'=> 2, 					// 	id=2 and slug='test'
			'slug' => 'test'
		));

		// this next one is dangerous and should only used for static
		// 	db values - i.e. no user input
		$test = new Test(				// will load test with id=2
			"id=2 OR slug='test'"		//	or slug='test'
		); 		


2. Creating

	In the BaseModel, there is a function called "assign", which 
	compares the params passed through an array variable to those
	in $_db_vars. If the variable is found in both the params sent
	and the $_db_vars, it is assigned to that variable. This is a 
	time saving method that allows for easier coding. Whether the 
	id is found, depends on whether you've set the '$assign_id' flag,
	which is set to false as default. It is not advised to set the
	'assign_id' flag to true, as BaseModel takes care of this flag
	automatically.

		$params = Request::getParams();
		$test = new Test();
		$test->assign($params, $assign_id = false);
		$test->create();

	An additional flag is accepted in the create function. If 

		$test->create($replace = true); 

	is set, it will use the mysql function "REPLACE INTO", instead 
	of "CREATE ".


3. Updating

	Very similar to create, the major difference is that it searches
	for a specific object in the database, and updates that based
	on the variables sent to 'assign'. It is not advisable to set 
	the 'assign_id' flag to 'assign', as the BaseModel takes care of 
	this automatically.

		$params = Request::getParams();
		$test = new Test($params['id']);
		$test->assign($params);
		$test->update();


4. Deleting 

	Very similar to loading, deleting an object simply a matter
	of calling the delete function.

		$params = Request::getParams();
		$test = new Test($params['id']);
		$test->delete();


5. Retrieving

	A specific function to this BaseModel, retrieving simply loads
	a specific list of objects according to the parameters involved.
	There are built in shortcuts that allow one-line operations, including 
	'init' and 'singleton', which perform similar functions. 

		// This creates a new Test object (no id) and retrieves all the 
		// objects possible according to parameters sent (in this case, none)
		Test::init()->retrieve();

		// This creates a new Test object (no id), if none has already been
		// initiated and retrieves all the objects possible according to 
		// parameters sent (in this case, none)
		Test::singleton()->retrieve();

	The different parameters that can be sent to retrieve:

		// load all tests where 'category = 3', optionally, 
		//	can parameters to array
		Test::init()->retrieve(array('category' => 3));

		// load all test objects where title contains 'test'
		Test::init()->retrieve("title like '%test%'");

6. Pagination

	When dealing with a MySQL app, pagination is often required, so a
	function was added to the BaseModel that acts like retrieve, but
	will only retrieve a specific number of items, put it into a 
	specifically formatted array and also tell you how many items 
	there are in total. 

	It uses the same parameters as retrieve, but takes additional ones 
	detailing how many to retrieve and which index to start at.

	The defaults are set at

		App::config()->PAGINATION

	Pagination can be called via

		Test::init()->getPaginated(array('category' => 3), $params);

	The params will be validated against 
	
		/framework/helpers/Pagination.class.php, 
	
	which contains the defaults from the configuration files and some 
	validation.

	The payload looks like: 

	(object)array(
        'items' 	=> array(), 
        'total' 	=> {total_amount},
        'params' 	=> $params
    );


* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 


##Error Handling

To do later.
