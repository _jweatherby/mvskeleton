<?php

class App
{
	private static $_config = array();

	public static function init($app_path, $app_type, $section = false)
	/*
		@params
			$app_path - the directory where the mvc can be found
			$app_type - directory 'framework/{$type}' to be loaded.  
			 		   		Includes custom files to load for that type of app 
			 		   		(i.e. basemodel, custom basecontrollers, DB, etc.)
			$section - the name of the config file, the controller directory and the view directory
			 			* case-sensitive (i.e. MAIN = MAIN OR main = main)
	*/
	{
		try
		{
			self::loadFramework(array( 
				"framework/core",
				$app_type ? "framework/$app_type" : false,
			));

			self::loadConfig("$app_path/config/", $section);
			self::loadDirs(array("$app_path/admin"));

			self::setEnv(self::config()->PHP);
			self::handleErrors();
		}
		catch(Exception $e)
		{
			App::catchError($e);
		}
	}

	public static function htmlRequest($params)
	{
		try
		{
			if(!isset($params['_r']))
				$params['_r'] = '';
			Request::singleton()->create(
				$router = Router::route($params['_r']) ,
				$params = $params,
				(isset($_SERVER['REQUEST_TYPE'])) && 
					($_SERVER['REQUEST_TYPE'] == 'POST') ? 'POST' : 'GET'
			);
			
			App::executeRequest();
		}
		
		catch(Exception $e)
		{
			App::catchError($e);
		}
	}

	private static function loadConfig($config_path, $section = false)
	{
		$default 	= $config_path."default.php";
		$env 		= $config_path."env.php";
		$keys 		= $config_path.'keys.php';

		if($section)
		{
			$config_file = strtolower($section).".php";
			$main 		= $config_path."$config_file";
		}
		/*
		if(!(is_file($main) && is_file($env) && is_file($keys)))
		{
			throw new ConfigException(
				"Config requires $config_file, env.php, keys.php in $config_path"
			);
		}
		*/
		self::$_config = (object)array_merge(
			require_once($default),
			is_file($env)  ? require_once($env)  : array(),
			is_file($keys) ? require_once($keys) : array(),
			is_file($main) ? require_once($main) : array()
		);

		spl_autoload_register('App::modelLoader');
	}

	public static function loadDirs($d)
	/* 
		@string d - an array of absolute path directories and 
			includes all the files ending with .php
	*/ 
	{
		foreach($d as $directory)
		{
			if(!$directory) continue; 

		    $currentd = dir(self::properPath($directory));
		    $files = array();

		    while (false !== ($filename = $currentd->read())){
		    	$files[] = $directory.DIRECTORY_SEPARATOR.$filename;
		    }
		    
		    asort($files);
		    
		    foreach($files as $filename)
		    {
		    	if (substr($filename, -4) == '.php')
		    	    require_once $filename;
		    }

		    $currentd->close(); 
		}
	}

	private static function catchError($e)
	{
		Request::singleton()->createError($e);
		App::executeRequest();
	} 

	private static function executeRequest()
	{
		$controller = Request::singleton()->ctrl_class;
		$action = Request::singleton()->action;

		$ctrl = new $controller();
		return $ctrl->$action();
	}

	private static function loadFramework($d)
	{	
		foreach($d as $directory)
		{
			if(!$directory) continue; 

			$directory = self::properPath($directory);
			$curr_path  = __DIR__.DIRECTORY_SEPARATOR.$directory;
		    $currentd = dir($curr_path);
		    $files = array();

		    while (false !== ($filename = $currentd->read())){
		    	$files[] = $curr_path.DIRECTORY_SEPARATOR.$filename;
		    }
		    
		    asort($files);
		    
		    foreach($files as $filename)
		    {
		    	if (substr($filename, -4) == '.php')
		    	    require_once $filename;
		    }

		    $currentd->close(); 
		}
		
		spl_autoload_register('App::helperLoader');

		if(is_file($vendor_autoload = 
			self::properPath(__DIR__."/vendor/autoload.php")))
		{
			require_once($vendor_autoload);
		}
	}

	private static function modelLoader($class)
	{
		if(!class_exists($class))
		{
			$to_load = explode("_", $class);
			$path = App::config()->APP_PATH . self::properPath('mvc/Models');

			foreach($to_load as $key=>$file)
			{
				$path .= DIRECTORY_SEPARATOR.ucfirst($file);
				if(is_file($path.".class.php")) 
					require_once($path.".class.php");
			}
		}
	}

	private static function helperLoader($class)
	{
		if(!class_exists($class))
		{
			$to_load = explode("_", $class);
			$path = self::properPath(__DIR__.'/framework/helpers');
			foreach($to_load as $key=>$file)
			{
				$path .= DIRECTORY_SEPARATOR.ucfirst($file);
				if(is_file($path.".class.php")) 
					require_once($path.".class.php");
			}
		}		
	}

	public static function config()
	{
		return self::$_config;
	}

	public static function getConfigPath($config_var)
	{
		if(!isset(self::$_config->$config_var))
			throw new ErrException("The config variable, $config_var does not ".
				"exist when compiling the full path");

		return App::config()->APP_PATH . 
			self::properPath(self::$_config->$config_var);
	}

	public static function properPath($path)
	{
		return implode(DIRECTORY_SEPARATOR, explode("/", $path));
	}

	public static function isDebugEnabled()
	{
		$envs = array('live', 'prod');

		return App::config()->DEBUG && !in_array(strtolower(App::config()->ENV), $envs);
	}

	public static function setEnv($settings)
	{
		if(is_array($settings) == false) 
			return false;
		
		foreach($settings as $var=>$val)
		{
			ini_set($var, $val);
		}
	}

	public static function handleErrors()
	{
		set_error_handler(function($errno, $errstr, $errfile, $errline, array $errcontext)
		{
		    if (0 === error_reporting()) {
		        return false;
		    }
		    $errfile = str_replace(App::config()->APP_PATH, "", $errfile);
	    	$msg = "In file: {$errfile} on line {$errline}: {$errstr}";

		    if(ob_get_level()) {
				    @ob_end_clean();
				//if(App::config()->DEBUG)
			    	throw new TplException($msg, 0, $errno, $errfile, $errline);
		    }
		    else {
			    throw new ErrException($msg, 0, $errno, $errfile, $errline);
		    }
		});
	}
}






