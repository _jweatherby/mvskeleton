<?php

/*

PHP Template Inheritance
------------------------
Version: 0.9
Released: Sun Mar 28 10:05:41 2010 -0700
Website: http://phpti.com/
Author: Adam Shaw (http://arshaw.com/)
Released under the MIT License (license.txt)

*/

class Ti
{

	private static $GLOBALS = array(
		'_ti_base' => null,
		'_ti_stack' => null
	);


	public static function emptyblock($name) {
		$trace = self::_ti_callingTrace();
		self::_ti_init($trace);
		self::_ti_insertBlock(
			self::_ti_newBlock($name, null, $trace)
		);
	}


	public static function start($name, $filters=null) {
		$trace = self::_ti_callingTrace();
		self::_ti_init($trace);
		$stack =& self::$GLOBALS['_ti_stack'];
		$stack[] = self::_ti_newBlock($name, $filters, $trace);
	}


	public static function end($name=null) {
		$trace = self::_ti_callingTrace();
		self::_ti_init($trace);
		$stack =& self::$GLOBALS['_ti_stack'];
		if ($stack) {
			$block = array_pop($stack);
			if ($name && $name != $block['name']) {
				self::_ti_warning("start('{$block['name']}') does not match end('$name')", $trace);
			}
			self::_ti_insertBlock($block);
		}else{
			self::_ti_warning(
				$name ? "orphan end('$name')" : "orphan end()",
				$trace
			);
		}
	}


	public static function superblock() {
		if (self::$GLOBALS['_ti_stack']) {
			echo self::getsuperblock();
		}else{
			self::_ti_warning(
				"superblock() call must be within a block",
				self::_ti_callingTrace()
			);
		}
	}


	public static function getsuperblock() {
		$stack =& self::$GLOBALS['_ti_stack'];
		if ($stack) {
			$hash =& self::$GLOBALS['_ti_hash'];
			$block = end($stack);
			if (isset($hash[$block['name']])) {
				return implode(
					self::_ti_compile(
						$hash[$block['name']]['block'],
						ob_get_contents()
					)
				);
			}
		}else{
			self::_ti_warning(
				"getsuperblock() call must be within a block",
				self::_ti_callingTrace()
			);
		}
		return '';
	}


	public static function flushblocks() {
		$base =& self::$GLOBALS['_ti_base'];
		if ($base) {
			$stack =& self::$GLOBALS['_ti_stack'];
			$level =& self::$GLOBALS['_ti_level'];
			while ($block = array_pop($stack)) {
				self::_ti_warning(
					"missing end() for start('{$block['name']}')",
					self::_ti_callingTrace(),
					$block['trace']
				);
			}
			while (ob_get_level() > $level) {
				ob_end_flush(); // will eventually trigger bufferCallback
			}
			$base = null;
			$stack = null;
		}
	}


	public static function blockbase() {
		self::_ti_init(self::_ti_callingTrace());
	}


	private static function _ti_init($trace) {
		$base =& self::$GLOBALS['_ti_base'];
		if ($base && !self::_ti_inBaseOrChild($trace)) {
			self::flushblocks(); // will set $base to null
		}
		if (!$base) {
			$base = array(
				'trace' => $trace,
				'filters' => null, // purely for compile
				'children' => array(),
				'start' => 0, // purely for compile
				'end' => null
			);
			self::$GLOBALS['_ti_level'] = ob_get_level();
			self::$GLOBALS['_ti_stack'] = array();
			self::$GLOBALS['_ti_hash'] = array();
			self::$GLOBALS['_ti_end'] = null;
			self::$GLOBALS['_ti_after'] = '';
			ob_start('Ti::_ti_bufferCallback');
		}
	}


	private static function _ti_newBlock($name, $filters, $trace) {
		$base =& self::$GLOBALS['_ti_base'];
		$stack =& self::$GLOBALS['_ti_stack'];
		while ($block = end($stack)) {
			if (self::_ti_isSameFile($block['trace'], $trace)) {
				break;
			}else{
				array_pop($stack);
				self::_ti_insertBlock($block);
				self::_ti_warning(
					"missing end() for start('{$block['name']}')",
					self::_ti_callingTrace(),
					$block['trace']
				);
			}
		}
		if ($base['end'] === null && !self::_ti_inBase($trace)) {
			$base['end'] = ob_get_length();
		}
		if ($filters) {
			if (is_string($filters)) {
				$filters = preg_split('/\s*[,|]\s*/', trim($filters));
			}
			else if (!is_array($filters)) {
				$filters = array($filters);
			}
			foreach ($filters as $i => $f) {
				if ($f && !is_callable($f)) {
					self::_ti_warning(
						is_array($f) ?
							"filter " . implode('::', $f) . " is not defined":
							"filter '$f' is not defined", // TODO: better messaging for methods
						$trace
					);
					$filters[$i] = null;
				}
			}
		}
		return array(
			'name' => $name,
			'trace' => $trace,
			'filters' => $filters,
			'children' => array(),
			'start' => ob_get_length()
		);
	}


	private static function _ti_insertBlock($block) { // at this point, $block is done being modified
		$base =& self::$GLOBALS['_ti_base'];
		$stack =& self::$GLOBALS['_ti_stack'];
		$hash =& self::$GLOBALS['_ti_hash'];
		$end =& self::$GLOBALS['_ti_end'];
		$block['end'] = $end = ob_get_length();
		$name = $block['name'];
		if ($stack || self::_ti_inBase($block['trace'])) {
			$block_anchor = array(
				'start' => $block['start'],
				'end' => $end,
				'block' => $block
			);
			if ($stack) {
				// nested block
				$stack[count($stack)-1]['children'][] =& $block_anchor;
			}else{
				// top-level block in base
				$base['children'][] =& $block_anchor;
			}
			$hash[$name] =& $block_anchor; // same reference as children array
		}
		else if (isset($hash[$name])) {
			if (self::_ti_isSameFile($hash[$name]['block']['trace'], $block['trace'])) {
				self::_ti_warning(
					"cannot define another block called '$name'",
					self::_ti_callingTrace(),
					$block['trace']
				);
			}else{
				// top-level block in a child template; override the base's block
				$hash[$name]['block'] = $block;
			}
		}
	}


	public static function _ti_bufferCallback($buffer) {
		try
		{
			$base =& self::$GLOBALS['_ti_base'];
			$stack =& self::$GLOBALS['_ti_stack'];
			$end =& self::$GLOBALS['_ti_end'];
			$after =& self::$GLOBALS['_ti_after'];
			if ($base) {
				while ($block = array_pop($stack)) {
					self::_ti_insertBlock($block);
					self::_ti_warning(
						"missing end() for start('{$block['name']}')",
						self::_ti_callingTrace(),
						$block['trace']
					);
				}
				if ($base['end'] === null) {
					$base['end'] = strlen($buffer);
					$end = null; // todo: more explanation
					// means there were no blocks other than the base's
				}
				$parts = self::_ti_compile($base, $buffer);
				// remove trailing whitespace from end
				$i = count($parts) - 1;
				$parts[$i] = rtrim($parts[$i]);
				// if there are child template blocks, preserve output after last one
				if ($end !== null) {
					$parts[] = substr($buffer, $end);
				}
				// for error messages
				$parts[] = $after;
				return implode($parts);
			}else{
				return '';
			}
		}
		catch(Exception $e)
		{
			return $e->getMessage();
		}
	}


	private static function _ti_compile($block, $buffer) {
		$parts = array();
		$previ = $block['start'];
		foreach ($block['children'] as $child_anchor) {
			$parts[] = substr($buffer, $previ, $child_anchor['start'] - $previ);
			$parts = array_merge(
				$parts,
				self::_ti_compile($child_anchor['block'], $buffer)
			);
			$previ = $child_anchor['end'];
		}
		if ($previ != $block['end']) {
			// could be a big buffer, so only do substr if necessary
			$parts[] = substr($buffer, $previ, $block['end'] - $previ);
		}
		if ($block['filters']) {
			$s = implode($parts);
			foreach ($block['filters'] as $filter) {
				if ($filter) {
					$s = call_user_func($filter, $s);
				}
			}
			return array($s);
		}
		return $parts;
	}


	private static function _ti_warning($message, $trace, $warning_trace=null) {
		
		if (error_reporting() & E_USER_WARNING) 
		{
			if (defined('STDIN')) {
				// from command line
				$format = "\nTemplate Inheritance Error: %s in %s on line %d\n";
			}else{
				// from browser
				$format = "<br />\n<b>Template Inheritance Error</b>:  %s in <b>%s</b> on line <b>%d</b><br />\n";
			}
			if (!$warning_trace) {
				$warning_trace = $trace;
			}
			
			$s = sprintf($format, $message, $warning_trace[0]['file'], $warning_trace[0]['line']);
			
			throw new Exception($s);
			//trigger_error($s, E_USER_WARNING);

			/*if (!self::$GLOBALS['_ti_base'] || self::_ti_inBase($trace)) {
				trigger_error($s, E_USER_WARNING);
			}else{
				trigger_error($s, E_USER_WARNING);
				//self::$GLOBALS['_ti_after'] .= $s;
			}*/
		}
	}


	/* backtrace utilities
	------------------------------------------------------------------------*/


	private static function _ti_callingTrace() {
		$trace = debug_backtrace();
		foreach ($trace as $i => $location) {
			if (isset($location['file']) && $location['file'] !== __FILE__) {
				return array_slice($trace, $i);
			}
		}
	}


	private static function _ti_inBase($trace) {
		return self::_ti_isSameFile($trace, self::$GLOBALS['_ti_base']['trace']);
	}


	private static function _ti_inBaseOrChild($trace) {
		$base_trace = self::$GLOBALS['_ti_base']['trace'];
		return
			$trace && $base_trace &&
			self::_ti_isSubtrace(array_slice($trace, 1), $base_trace) &&
			$trace[0]['file'] === $base_trace[count($base_trace)-count($trace)]['file'];
	}


	private static function _ti_isSameFile($trace1, $trace2) {
		return
			$trace1 && $trace2 &&
			$trace1[0]['file'] === $trace2[0]['file'] &&
			array_slice($trace1, 1) === array_slice($trace2, 1);
	}


	private static function _ti_isSubtrace($trace1, $trace2) { // is trace1 a subtrace of trace2
		$len1 = count($trace1);
		$len2 = count($trace2);
		if ($len1 > $len2) {
			return false;
		}
		for ($i=0; $i<$len1; $i++) {
			if ($trace1[$len1-1-$i] !== $trace2[$len2-1-$i]) {
				return false;
			}
		}
		return true;
	}
}