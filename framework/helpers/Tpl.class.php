<?php

class Tpl
{
	private static $base_dir; 

	public static function initRain($folder, $vars = array())
	{
		$base_dir = App::config()->APP_PATH.App::config()->VIEW_PATH;
		Rain\Tpl::configure(array_merge(
			App::config()->raintpl,
			array(
				"tpl_dir"=>App::properPath("{$base_dir}/{$folder}/"),
			)
		));
	}

	public static function init($req, $view_template = false)
	/**
	* 	@post gets file from VIEW_PATH or MOBILE_PATH set out in config
	*/
	{
		self::$base_dir = App::config()->APP_PATH.App::config()->VIEW_PATH;

		if(class_exists("Mobile_Detect") && !empty(App::config()->MOBILE_PATH))
		{
			$detect = new Mobile_Detect();
			if($detect->isMobile() == true && $detect->isTablet() == false)
				self::$base_dir = App::config()->APP_PATH . 
                    App::config()->MOBILE_PATH;
		}

		if($view_template/* && self::checkFile($view_template)*/){
	    	$view_template = preg_replace("/\.php$/", "", $view_template); 
			self::getFile($view_template.".php");
		}

		else	{
			self::getFile(ucfirst($req->controller)."/{$req->action}.php");
		}
	}

	public static function getLayout($file, $vars = array())
	/**
	* 	@post gets file from VIEW_PATH or MOBILE_PATH set out in config
	*/
    {
    	$file = preg_replace("/\.php$/", "", $file); 
        self::getFile("Layout/{$file}.php", $vars);
    }

    public static function insert($file, $vars = array())
	/**
	* 	@post gets file from VIEW_PATH or MOBILE_PATH set out in config
	*/
    {
    	$file = preg_replace("/\.php$/", "", $file); 
    	self::getFile(ucfirst("$file.php"), $vars);
    }

    public static function getError($file, $vars = array())
    {
    	$file = preg_replace("/\.php$/", "", $file); 
    	self::getFile(App::config()->ERROR_PATH . $file . ".php", $vars);
    }

    public static function getScript($script)
    {
    	echo '<script src="'.$script.'"></script>';
    }

    private static function getFile($path, $vars = array())
    /**
	* 	@pre gets file from VIEW_PATH or MOBILE_PATH set out in config
	*/
    {	
    	$base_dir = self::$base_dir; //App::config()->VIEW_PATH;

    	// easily retrievable parameters
    	$_req = Request::singleton();
    	$_data = $_req->payload;
    	$_params = $_req->params; 

    	if(count($vars))
	    	foreach($vars as $var => $val)
	    		$$var = $val;

    	// standard path, i.e. Blog/home.php
    	
    	$compiled_path = $base_dir . $path;

    	// check if it is an "absolute" path from app root
    	// i.e. mvc/Views/Errors/404.php 
    	// if so, override the path with new path
    	
	    if(strpos($path, 'mvc/Views') !== false) {
    		$compiled_path = App::config()->APP_PATH.$path;
	    }

	    if(is_file($file = App::properPath($compiled_path))) {
			include($file);
		}
		
		else {
			include(App::properPath(__DIR__."/Error.tpl.php"));
			die;
		}
    }

    private static function checkFile($path)
    {
    	$base_dir = App::config()->VIEW_PATH;

    	if(preg_match("/[a-z]+\/[a-z]+/", $path) && 
    		is_file($base_dir.$path.".php"))
    	{
    		return true;
    	}

    	else
    	{
    		return false;
    	}
    }
}
