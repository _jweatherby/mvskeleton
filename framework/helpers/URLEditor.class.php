<?php

class URLEditor
{
	public static function stripVar($link, $el)
	{
		$query = parse_url($link, PHP_URL_QUERY);
		
		$vars = array();
		parse_str($query, $vars);
		
		unset($vars[$el]);
		
		$new_query = http_build_query($vars);

		return parse_url($link, PHP_URL_PATH) . 
			(strlen($new_query) == 0 ? '' : "?".$new_query);
	}

	public static function addVar($link, $el, $val)
	{
		$query = parse_url($link, PHP_URL_QUERY);
		
		$vars = array();
		parse_str($query, $vars);
		
		$vars[$el] = $val;
		
		$new_query = http_build_query($vars);

		return parse_url($link, PHP_URL_PATH) . 
			(strlen($new_query) == 0 ? '' : "?".$new_query);
	}

	public static function edit($link, array $cmds)
	{
		$query = parse_url($link, PHP_URL_QUERY);
		
		$vars = array();
		parse_str($query, $vars);
		
		if(is_array($cmds['add']) && count($cmds['add']))
		{
			foreach($cmds['add'] as $var=>$val)
			{
				$vars[$var] = $val;
			}
		}

		if(is_array($cmds['remove']) && count($cmds['remove']))
		{
			foreach($cmds['remove'] as $val)
			{
				unset($vars[$val]);
			}
		}
		
		$new_query = http_build_query($vars);

		return parse_url($link, PHP_URL_PATH) . 
			(strlen($new_query) == 0 ? '' : "?".$new_query);
	}
}