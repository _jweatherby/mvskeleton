<?php

class Utils
{	
	public static function required($array, $vars)
	{
		$msg='';

		if(!is_array($array)) 
			return "INVALID INPUT ENTERED TO REQUIRED";

		foreach($vars as $var)
			if(!isset($array[$var])) 
				$msg .= " " .$var;  

		if($msg) return "REQUIRED: ".$msg;

		else return false;

		return $msg;
	}

	public function curl($url)
	{
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-type: application/json'));
		//curl_setopt($ch, CURLOPT_TIMEOUT_MS, 10000);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
		$data = curl_exec($ch);
		curl_close($ch);
		return json_decode($data);
	}

	public static function dump($var)
	{
		echo '<pre>';
		print_r($var);
		echo '</pre>';
	}

	public static function strRand($length=32, $str = "")
	{
		if(empty($str))
			$chars = '0123456789abcdefghijklmnopqrstuvwxyz'.
			'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
		else
			$chars = $str;

    	$new_str = '';
    	
    	for($i=0; $i < $length; $i++)
    	{
    		$new_str .= $chars[rand(0,strlen($chars)-1)];
    	}
    	return $new_str;
	}
    public static function getVars($obj)
    {
    	return get_object_vars($obj);
    }

    public static function sqlDate($date=false)
    {
    	if($date)
    		return @date("Y-m-d H:i:s", $date);
    	else
    		return @date("Y-m-d H:i:s", @time());
    }

    public static function paginate($params)
	{
		$new = array();
		$new['batch'] = 10;
		$new['start'] = 0;

		// set page number to retrieve - default 0;
		if(is_numeric($params['start']))
			$new['start'] = (int)$params['start'];
		
		if(is_numeric($params['start']) && is_numeric($params['end']))
			if((int)$params['end'] > (int)$new['start'])
				$new['batch'] = (int)$params['end'] - (int)$new['start'] + 1;

		if(is_numeric($params['batch']))
			$new['batch'] = (int)$params['batch'];

		if(is_numeric($params['page'])){
			if(!is_numeric($params['batch']))
				$params['batch'] = $new['batch'];
			$new['start'] = ((int)$params['page']-1) * (int)$params['batch'];
		}
 		return $new;
	}

	public static function rmNl(&$string)
	{
		return $string = preg_replace("/[\r\n\t]+/", " ", $string);
	}

	public static function crop($item, $length=100)
	{
		$item = preg_replace("/<[^>]+>/", " ", $item);
		$new = preg_replace("/^(.{".$length."}\S*).*$/", "$1", $item);

		if(strlen($new) < strlen($item))
			return preg_replace("/\S+$/", "...", $new);

		return $new;
	}

	public static function matchEmail($item)
	{
		if(preg_match('/^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/', $item))
			return true;

		return false;
	}

    public static function formatBytes($size, $precision = 2)
	{
		$unit=array('b','kb','mb','gb','tb','pb');
	    return @number_format($size/pow(1024,
	    	($i=floor(log($size,1024)))),$precision).' '.$unit[$i];
	}

	public static function assocShuffle(&$array)
	{
		$keys = array_keys($array);
		shuffle($keys);
		$new = array();
		foreach($keys as $key)
			$new[$key] = $array[$key];
		return $array = $new;
	}

	/** cleaning functions */

	public static function redir($path)
	{
		if(!empty($path))
		{
			header("Location: $path");
			die;
		}
	}

	public static function createSlug($slug)
	{
		return strtolower(
			preg_replace('/[\.\,\!\?\'\"\:]+/', '', 
				preg_replace("/([\s\-]+)/", "_", $slug)));
	}

	public static function uncreateSlug($slug)
	{
		return preg_replace("/_/", " ", $slug);
	}

	public static function sortByVar(&$all, $var='id', $dir='ASC')
	{
		if(empty($all) || is_array($all) == false) return false;

		$tmp = array();

		foreach($all as $key=>$val) 
			$tmp[$key] = $val->$var; 

		return array_multisort($tmp, $dir == 'ASC' ? SORT_ASC : SORT_DESC, $all);
	}
}