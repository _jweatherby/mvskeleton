<?php

class CSV
{
	private $_file; 

	public function __construct($filename)
	{
		header("Content-type: application/csv");
		header("Content-Disposition: attachment; filename=".$filename);

        $this->_file = fopen('php://output', 'w');
	}

	public function writeLine($arr)
	{
		return fputcsv($this->_file, $arr);
	}

	public static function addBlanks(array &$array, $count)
	{
		for($i=0; $i < $count; $i++)
		{
			array_push($array, '');
		}
	}
}