<?php 

$e = Request::getException(); 
$req = Request::singleton();

?>

<html>
	
	<head>
		<title>Template not found</title>
	</head>

	<body> 

		<p></p>

		<h2>500 - System Error</h2>

		<p></p>

		<p>
			Please report this to our <a href="mailto:jamie.weatherby@gmail.com">resident genius</a>, and quote this message: 
			<br><br>
			&nbsp;&nbsp;&nbsp;&nbsp;
			<i>"The page at <strong><?= preg_replace("/^\/.*\/$/", "", $_SERVER['REQUEST_URI']); ?></strong>, could not be found."</i>
		</p>

		<?php if(App::isDebugEnabled() && !empty($e)){ ?>

		<h3>Note: If you're in this file, it means a standard error template could not be found.<br>
			Check that your VIEW_PATH and ERROR_PATH are pointing to the correct locations.</h3>
		<div class="side-margin">

			<p>
				<strong>Code:</strong>
				<?= $e->getCode(); ?>
			</p>

			<p>
				<strong>Message:</strong> 
				<?= $e->getMessage(); ?>
			</p>

			<p>
				<strong>Backtrace:</strong>
				<pre><?php debug_print_backtrace(); ?></pre>
			</p>

		</div>

		<?php } ?>
		
	</body>
	
</html>