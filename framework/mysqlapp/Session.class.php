<?php

class Session
{
    private static $instance;
    private $_db; 
    
    public function __construct() 
    {   
        $this->_db = DB::singleton(); 

        session_set_save_handler( 
            array( &$this, "_open" ), 
            array( &$this, "_close" ),
            array( &$this, "_read" ),
            array( &$this, "_write"),
            array( &$this, "_destroy"),
            array( &$this, "_clean")
        );
    } 

    public static function start()
    {
        if(!isset(self::$instance))
            self::$instance = new Session();

        return self::$instance;
    }
    
    public function _open()
    {
        
    }
    
    public function _close()
    {
        
    }
    
    public function _read($id)
    {
        $query = "SELECT data FROM sessions WHERE id = :id"; 
        $sql = $this->_db->prepare($query);
        $sql->bindValue(":id",$id,PDO::PARAM_STR);
        $sql->execute();
        
        return $sql->fetch()->data;    
    }
    
    public function _write($id, $data)
    {
        $last_access = time();
        if(empty($data)) return false;
        
        $query = "INSERT INTO sessions (id, last_access, data) 
            VALUES 
                (:id, :last_access, :data) 
            ON DUPLICATE KEY 
            UPDATE data=:data, last_access=:last_access";
        
        $sql = $this->_db->prepare($query);
        $sql->bindValue(":id",$id,PDO::PARAM_INT);
        $sql->bindValue(":last_access",$last_access,PDO::PARAM_STR);
        $sql->bindValue(":data",$data,PDO::PARAM_STR);
        $sql->execute();
        return true; 
    }
    
    public function _destroy($id)
    {
        $query = "DELETE FROM sessions WHERE id=:id";
        $sql = $this->_db->prepare($query);
        $sql->bindValue(":id",$id,PDO::PARAM_INT);
        $sql->execute();
        return true;    
    }
    
    public function _clean($max)
    {
        $old = time() - 3600;
        
        $query = "DELETE FROM sessions WHERE last_access < :old";
        $sql = $this->_db->prepare($query);
        $sql->bindValue(":old",$old,PDO::PARAM_INT);
        $sql->execute();
        return true;
    }

    public static function getUser($id)
    {
        $query = "SELECT user_id FROM sessions WHERE id=:id";
        $sql = $this->_db->prepare($query);
        $sql->bindValue(":id",$id,PDO::PARAM_STR);
        $sql->execute();
        return $sql->fetch()->user_id;
    }
    
}