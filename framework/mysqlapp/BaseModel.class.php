<?php

abstract class BaseModel
{
	protected $_db, $_table, $_db_vars;

	protected static $instance;

	public static function singleton($id=false)
	{
		$class = get_called_class();
		
		if(!isset(self::$instance) || 
			$class != get_class(self::$instance))
			self::$instance = new $class($id);

		return self::$instance;
	}

	public static function init($id=false)
	{
		$class = get_called_class();
		
		return new $class($id);
	}

	public function __construct($id=false)
	{
		$this->_db = DB::singleton();
		
		if(empty($this->_table))
			$this->_table = strtolower(get_class($this));

		if($id)	$this->load($id);
	}

	public function setDBVars(array $array)
	{
		if(is_array($this->_db_vars))
			$this->_db_vars = array_merge($this->_db_vars, $array);

		else
			$this->_db_vars = $array;
	}

	public function getDBVars()
	{
		return $this->_db_vars;
	}

	private static function getVars($obj)
	{
		return get_object_vars($obj);
	}

	public static function buildQuery($query, $where=false, $extras='')
	{
		if(!is_string($extras)) $extras = '';

		if(is_numeric($where))
		{
			$query .= " WHERE id=:id ".$extras;
			$sql = DB::singleton()->prepare($query);
			$sql->bindValue(":id", $where, PDO::PARAM_INT);
		}

		elseif(is_string($where) && 
			preg_match("/.*([=<>]|\slike\s).*/i", $where))
		{
			$query .= " WHERE ".$where." ".$extras;
			$sql = DB::singleton()->prepare($query);
		}

		elseif(is_array($where))
		{
			$query .= " WHERE ";
			foreach($where as $var=>$val)
				$query .= "{$var}=:$var AND ";
			
			$query = preg_replace("/AND\s$/", " ", $query).$extras;

			$sql = DB::singleton()->prepare($query);

			foreach($where as $var=>$val)
				$sql->bindValue(":$var", $val, 
					is_numeric($val) ? PDO::PARAM_INT : PDO::PARAM_STR);
		}

		elseif(empty($where))
		{
			$query .= " ".$extras;
			$sql = DB::singleton()->prepare($query);
		}

		else
			throw new PDOException(
				"<pre>Invalid query conditions sent to BaseModel\n". 
				print_r(array(
						'query'	=> $query, 
						'where'	=> $where,
						'extras'=> $extras
					),1).
			"</pre>");

		return $sql;

	}

	public function assign($data, $assign_id=false)
	{
		if(is_object($data))
			$data = self::getVars($data);

		if(is_array($data))
		{
			$vars = self::getVars($this);

			foreach($vars as $var=>$val)
			{
				if(isset($data[$var]) && 
					($var != 'id' || $assign_id==true)
				)
				{
					$this->$var = $data[$var];
				}
			}
		}		
	}

	public function load($where)
	{
		if(is_numeric($where) == false && 
			is_array($where)==false && 
			preg_match(App::config()->SLUG_CHARS, $where) &&
			in_array('slug', $this->_db_vars))
		{
			$where = "slug='$where'";
		}

		$sql = self::buildQuery("SELECT * FROM {$this->_table}", $where);
		$sql->execute();

		$sql->setFetchMode(PDO::FETCH_CLASS, get_class($this));
		$this->assign($sql->fetch(), $assign_id = true);

		return $this;
	}	

	public function delete($where = false)
	{
		$sql = self::buildQuery("DELETE FROM {$this->_table}", 
			$where ? $where : $this->id);

		$sql->execute();
		
		$this->clear();

		return true;
	}

	public function update($where = false)
	{
		$vars = self::getVars($this);
		$updating = array();

		foreach( $vars as $var => $val )
		{
			if((is_string($this->$var) || is_numeric($this->$var)) && 
				(!is_array($this->_db_vars) || in_array($var,$this->_db_vars) &&
				property_exists($this, $var))
			)
			{
				$to_update .= "`".$var."`=:".$var.",";
				$updating[] = $var;				
			}
		}

		$to_update = preg_replace("/,$/", "", $to_update);

		$query = "UPDATE {$this->_table} SET ".$to_update;
		
		$sql = self::buildQuery($query, $where ? $where : $this->id);

		foreach($updating as $var)
		{
			$sql->bindValue(":".$var, $this->$var, 
				is_numeric($this->$var) ? PDO::PARAM_INT : PDO::PARAM_STR);
		}
			
		$sql->execute();

		return $this->load($where ? $where : $this->id);
	}

	public function create($replace = false)
	{
		$vars = self::getVars($this);
		$adding = array();
		$cols = "(";
        $vals = "(";

        foreach($vars as $var=>$val)
        {
        	if(
				(is_string($this->$var) || 
					is_numeric($this->$var)) && 
				property_exists($this, $var) &&
				(!is_array($this->_db_vars) || 
					in_array($var, $this->_db_vars))
			)
        	{
                $cols .= "`{$var}`,";
                $vals .= ":{$var},";
                $adding[] = $var;
            }
        }

        $cols = substr($cols, 0, strlen($cols)-1).")";
        $vals = substr($vals, 0, strlen($vals)-1).")";

		$query = ($replace ? "REPLACE" : "INSERT IGNORE"). 
			" INTO `{$this->_table}` {$cols} VALUES {$vals}";

		$to_update = preg_replace("/,$/", "", $to_update);

		$sql = DB::singleton()->prepare($query);

		foreach($adding as $var)
		{
			$sql->bindValue(":".$var, $this->$var, 
				is_numeric($this->$var) ? PDO::PARAM_INT : PDO::PARAM_STR);
		}
			
		$sql->execute();

		if($sql->rowCount())
		{
			if(in_array('id', array_keys($vars)))
			{
				$this->id = $this->_db->lastInsertId();
				$this->load($this->id);
			}
		}

		return $this;
	}

	public function retrieve($where = false)
	{
		$sql = self::buildQuery("SELECT * FROM {$this->_table}", $where);
		$sql->execute();
		$sql->setFetchMode(PDO::FETCH_CLASS, get_class($this));
		
		return $sql->fetchAll();
	}

	public function getPaginated($where = false, $params = false)
	{
		$params = Pagination::setParams($params);
		
		$limits = " ORDER BY {$params['sort']} {$params['dir']}
			LIMIT {$params['start']},{$params['batch']}";

		$sql = self::buildQuery("SELECT SQL_CALC_FOUND_ROWS * 
			FROM {$this->_table}", $where, $limits);

		$sql->execute();

		$sql->setFetchMode(PDO::FETCH_CLASS, get_class($this));
		
	  	$all = (object)array(
            'items' 	=> array(), 
            'total' 	=> DB::singleton()
                    		->query('SELECT FOUND_ROWS();')
                    		->fetch(PDO::FETCH_COLUMN),
            'params' 	=> $params
        );

        while($load = $sql->fetch())
        {
            if(!empty($load->id))
                $all->items[$load->id] = $load; 
	        else
                $all->items[] = $load; 
        }
        
        return $all;	
	}

	public function clear()
	{
		$vars = self::getVars($this);

		foreach($vars as $key=>$val)
			$this->$key = NULL;
	}

	public static function buildWhere($items, $str = false, $and_where = " AND ")
	{
		if(empty($str))
			$str = get_class($this)."_id";
		$q_where = " $and_where (";

		foreach($items as $item)
			$q_where .= "$str={$item->id} OR ";
		
		return preg_replace("/\sOR\s$/", ")", $q_where);
	}

}