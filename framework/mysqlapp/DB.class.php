<?php

class DB extends PDO
{
	private static $instance;
	
	public static function singleton()
	{
		if(!isset(self::$instance))
			self::$instance = new DB();
		
		return self::$instance;
	}
	
	public function __construct() 
	{  
        parent::__construct(
        	"mysql:host=".App::config()->DB['HOST'].
        	";dbname=".App::config()->DB['DATABASE'], 
        	App::config()->DB['USERNAME'], 
        	App::config()->DB['PASSWORD']
        );

        $this->query("SET NAMES utf8;SET time_zone='Europe/London'");
        $this->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);  
        $this->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ);
	}
}