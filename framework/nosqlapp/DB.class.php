<?php

class DB
{
	private static $_cached = array();

	public static function write($json_file)
	{
		$filepath = App::config()->APP_PATH.
			App::config()->JSON['path']."{$json_file}.json"; 

		if(is_writable($file = App::properPath($filepath)))
			return file_put_contents($file, 
				json_encode(self::$_cached[$json_file]));
		
		else
			throw new JSONException("$file not found or is not writable.");
	}

	public static function read($json_file, $class = false)
	{
		if(isset(self::$_cached[$json_file]))
			return self::$_cached[$json_file];
		
		$filepath = App::config()->APP_PATH.
			App::config()->JSON['path']."{$json_file}.json"; 
		
		if(is_file($file = App::properPath($filepath)))
		{
			$contents = json_decode(file_get_contents($file));
			
			if(is_object($contents)) 
				$contents = array_values(get_object_vars($contents));

			return self::$_cached[$json_file] = $contents;
		}
		
		else
			throw new JSONException("$file not found");
	}

	public static function overwrite($obj, $key)
	{
		foreach(self::$_cached[$key] as $i=>$item)
		{
			if($item->id == $obj->id)
				self::$_cached[$key][$i] = $obj; 		
		}
	}

	public static function add($obj, $key)
	{
		self::read($key); 
		
		if(!empty(self::$_cached[$key]))
			self::$_cached[$key][] = $obj; 

		else
			self::$_cached[$key] = array($obj);
	}

	public static function remove($obj, $key)
	{
		foreach(self::$_cached[$key] as $i=>$item)
		{
			if($item->id == $obj->id)
				unset(self::$_cached[$key][$i]); 		
		}		
	}
}