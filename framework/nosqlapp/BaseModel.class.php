<?php

abstract class BaseModel
{
	protected static $instance;
	protected $_table;

	protected static function getVars($obj)
	{
		return get_object_vars($obj);
	}

	public static function init($id=false)
	{
		$class = get_called_class();
		return new $class($id);
	}

	public function assign($data, $assign_id=false)
	{
		if(is_object($data))
			$data = self::getVars($data);

		if(is_array($data))
		{
			$vars = self::getVars($this);

			foreach($vars as $var=>$val)
			{
				if(isset($data[$var]) && 
					($var != 'id' || $assign_id==true)
				)
				{
					$this->$var = $data[$var];
				}
			}
		}		
	}

	public function __construct($id = false)
	{
		if(empty($this->_table))
			$this->_table = strtolower(get_class($this));

		if($id)	$this->load($id);
	}

	public function load($id)
	{
		$contents = DB::read($this->_table);
		foreach($contents as $obj)
		{
			if(self::checkWhere($id,$obj,get_class($this)))
			{
				$this->assign($obj, $assign_id = true);
			}
		}
	}

	public function save()
	{
		if($this->id)
			DB::overwrite($this, $this->_table);
		
		else
		{
			$this->id = $this->generateId();
			DB::add($this, $this->_table);
		}
		
		DB::write($this->_table);
	}

	public function create()
	{
		$this->id = $this->generateId();
		DB::add($this, $this->_table);
		DB::write($this->_table);
	}

	public function update()
	{
		if($this->id)
			DB::overwrite($this, $this->_table);
		
		DB::write($this->_table);
	}

	public function delete()
	{
		DB::remove($this, $this->_table);
		DB::write($this->_table);
	}

	public function retrieve($where = array())
	{
		$contents = DB::read($this->_table);
		$class = get_class($this);
		$all = array();

		foreach($contents as $key=>$obj)
		{	
			if(empty($where) || self::checkWhere($where,$obj,$class)) 
			{
				$all[$key] = new $class();
				$all[$key]->assign($obj, $assign_id = true); 				
			}
		}

		return $all;
	}

	private static function checkWhere($where,$obj,$class)
	{
		if(is_array($where))
		{
			foreach($where as $var=>$val)
			{
				if(property_exists($class, $var) && 
					((is_array($obj->$var) && in_array($val, $obj->$var)) || 
					($obj->$var == $val)) == false)
				{
					return false;
				}
			}

			return true;
		}
		
		else
		{
			if((is_numeric($where) && $obj->id == $where) || 
				(isset($obj->slug) && is_string($where) && $obj->slug == $where))
			{
				return true;
			}
			
			else 
			{
				return false;
			}
		}
	}

	private function generateId()
	{
		$all = DB::read($this->_table);
		$max_id = 0; 

		if(empty($all)) return 1;
		
		foreach($all as $item)
		{
			if($item->id > $max_id)
				$max_id = $item->id;
		}
		return $max_id + 1;
	}

	public function isUnique($var, $val)
	{
		$all = $this->retrieve();
		if(empty($all)) return true;

		foreach($all as $item)
		{
			if($item->$var == $val)
			{
				return false;
			}
		}

		return true;
	}
}