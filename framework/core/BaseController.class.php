<?php

abstract class BaseController
{
	private $view_template; 

	public function getView($payload = null, $view_template = false)
	{
		Request::setPayload($payload); 
		
		$req = Request::singleton();

		switch($req->dt)
		{
			case 'json':

				header('Cache-Control: no-cache, must-revalidate');
				header('Content-type: application/json; charset=utf-8');
				
				print_r($req::toJSON());


				return 'json';

			break;

			case 'csv':

				header("Content-type: application/csv; charset=utf-8");
				return 'csv';

			break;

			case 'xml':
			break;

			case 'rss':
			break;

			default:

				header("CONTENT-TYPE: text/html; charset=utf-8");

				Tpl::init($req, $view_template);	

				return 'html';

			break;
		} 
	}
}