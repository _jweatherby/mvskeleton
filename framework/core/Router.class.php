<?php

class Router
{
	public static function route($_r)
	{
		$nav = array(
			'controller' 	=> App::config()->DEFAULT_CONTROLLER,
			'action'		=> '',
			'id'			=> '',
			'dt'			=> 'html'
		);

		$viewTypes = App::config()->VIEW_TYPES;

		// get return view
		if(preg_match("/^.*\.([\w\-]+)$/", $_r, $dt))
		{
			if(in_array($dt[1], $viewTypes))
				$nav['dt'] = $dt[1];
		}
		
		elseif(isset($_REQUEST['dt']) && in_array($_REQUEST['dt'], $viewTypes))
		{
			$nav['dt'] = $_REQUEST['dt'];
		}
		
		$_r = preg_replace("/\.([\w\-]+)$/", "", $_r);

		foreach(App::config()->ROUTER as $url_match=>$dest)
		{
			$pattern = self::translate(explode("/", $url_match));
			if(preg_match($pattern, $_r))
			{
				return self::extract(preg_replace($pattern, $dest, $_r), $nav);
			}
		}

		throw new Exception("The router could not match that URL", 404); 
	}

	private static function translate(array $url, $i=0)
	{
		$pattern = false;
		
		if(isset(App::config()->ROUTER_SHORTCUTS[$url[$i]]) && 
			$shortcut = App::config()->ROUTER_SHORTCUTS[$url[$i]])
		{
			$pattern = $shortcut;
		}
		
		elseif(!empty($url[$i])) {
			$pattern = $url[$i];
		}

		$url[$i] = $pattern ? $pattern : $url[$i];

		if(array_key_exists($i+1, $url) && empty($url[$i+1]) == false)
		{
			return self::translate($url, $i+1);
		}

		else
		{
			return "/^".implode("\/", $url)."\/?$/";
		}
	}

	private static function extract($dest, &$nav)
	{
		$dest = explode("/", $dest);
		for($i = 0; $i < count($dest); $i++)
		{
			$found = array();
			if(preg_match("/^\{([\w\-]+):([\w\-]+)\}$/", $dest[$i], $found))
			{
				$nav[$found[1]] = $found[2];
			}
		}
		return $nav;
	}
}