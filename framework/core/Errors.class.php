<?php

class ErrorsController extends BaseController
{
	public $e;

	public function home()
	{
		$e = Request::getException();
		
		if(get_class($e) == 'ImageException')
		{
			return $this->getView(NULL, "Images/upload");
		}

		switch($e->getCode())
		{
			case 401:

			$_SESSION['CALLBACK_MSG'] = 
				'You need to be logged in to do that';
				
			if(Request::getViewFormat() == 'html')
				$_SESSION['CALLBACK'] = $_SERVER['REQUEST_URI'];

			else
				$_SESSION['CALLBACK'] = '/';
		}

		if(Request::getViewFormat() == 'html' && $e->getCode() != 401)
		{
			$code = $e->getCode();
			if(!empty($code))
				header("HTTP/1.0 ".$code);
		}
		return $this->getView(null, App::config()->ERROR_PATH.'home.php');
	}

}