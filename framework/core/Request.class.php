<?php

class Request
{
	public 

		// Request
		$request_type,
		$routed_request,
		$controller, 
		$action,
		$dt, 

		// current user
		$curr_user,

		// Parameters
		$params = array(),
		
		// Result
		$duration = 0,
		$success = true,
		$code = 200, 
		$msg = 'SUCCESS',
		$payload,
		$redir, 
		$logs = array();

	private $session;


	private static $instance, $e;

	public static function singleton()
	{
		if(!isset(self::$instance))
			self::$instance = new Request();
		
		return self::$instance;
	}

	public function log($msg)
	{
		$logs[] = $msg; 
	}
	
	public static function toJSON()
	{
		$req = self::$instance;
		if(App::isDebugEnabled()) {
			return json_encode($req); 
		}
		else {
			return json_encode(array(
				'success' => $req->success,
				'msg' => $req->msg,
				'payload' => $req->payload,
				'params' => $req->params
			));
		}
	}

	/* POST, GET */
	public function create(array $router, $params = false, $request_type = false)
	{
		if(empty($this->duration))
			$this->duration = microtime();

		if(empty($request_type) == false)
			$this->request_type = $request_type;
		
		if(empty($params) == false)
		{
			$this->routed_request = isset($params['_r']) ? $params['_r'] : '';
			$this->setParams($router, $params);
		}
		
		$this->dt = $router['dt'];

		$this->setController($router['controller']);	
		$this->setAction($router['action'], $router['id'], $router['slug']);
	}

	public function createError($e)
	{
		$ext = ""; 
		if(isset($_REQUEST['_r']))
			$ext = pathinfo($_REQUEST['_r'], PATHINFO_EXTENSION);
		
		$this->create(Router::route("errors" . ($ext ? '.'.$ext : '')));
		
		self::$e = $e;
		$this->success = false;
		$this->err_type = preg_replace("/Exception$/", "", get_class($e));
		$this->code = $e->getCode();
		if($this->err_type == 'Err')
			$this->msg = $e->getError(); 
		else
			$this->msg = $e->getMessage();
	}

	private function setController($controller)
	{
		$ctrl_class = ucfirst(strtolower($controller))."Controller";

		if(class_exists($ctrl_class))
		{
			$this->controller = ucfirst($controller);
			return $this->ctrl_class = $ctrl_class;
		}

		$ctrl_file = ucfirst($controller).".class.php";

		$root_path = App::properPath(
			App::config()->APP_PATH.App::config()->CTRL_PATH
		);

		if(is_file($ctrl = $root_path.DIRECTORY_SEPARATOR.$ctrl_file))
		{
			require_once($ctrl);
			$this->controller = ucfirst($controller); 
			return $this->ctrl_class = $ctrl_class;
		}

		else
			throw new Exception("File not found: $ctrl", 404);
	}

	private function setAction($action, &$id = false, &$slug = false)
	{
		$controller = $this->ctrl_class;

		$action = strtolower($action);

		if(empty($action) && empty($id) && empty($slug))
			return $this->action = 'home';

		elseif(method_exists($controller, $action))
			return $this->action = $action;

		elseif(method_exists($controller, 'view') && 
			(empty($id) == false || empty($slug) == false))
		{
			$this->action = 'view';
		}

		else
			throw new Exception("Action not found", 404);
	}

	private function setParams($router, $params)
	/*
	 	@params contains the contents of either $_REQUEST 
	*/
	{

		$ignore = array("controller", "action", "dt");
		self::clean($router);

		foreach($router as $var=>$val)
		{
			if(in_array($var, $ignore))
			{
				if(empty($this->$var))	$this->$var = $val;
			}
			
			else
			{
				$this->params[$var] = $val;
			}
		}
		
		$tags = self::getStripTags($this->controller, $this->action);

		if(empty($params) == false)
		{
			//self::clean($params, $tags);

			foreach($params as $var => $val)
			{
				if(!isset($this->$var) && 
					$var != '_r' && 
					$var != 'PHPSESSID' &&
					in_array($var, $ignore)==false &&
					strpos($var, "__") === false)
				{
					$this->params[$var] = $val;
				}
			}
		}
	}

	public static function setPayload($data)
	{
		$req = Request::singleton();
		$req->payload = $data;
		$req->duration = microtime() - $req->duration; 
		$req->peak_usage = Utils::formatBytes(memory_get_peak_usage());
		$req->session = isset($_SESSION) ? $_SESSION : array();
		
	}

	public static function getException()
	{
		return self::$e;
	}

	public static function getViewFormat()
	{
		return Request::singleton()->dt;
	}

	public static function getParam($var, $clean = true, $strip_tags = '')
	{
		$req = Request::singleton();
		if($clean) 
			return self::clean($req->params[$var], $strip_tags);
		else 
			return $req->params[$var];
	}

	public static function getParams($clean = true, $strip_tags = '')
	{
		$req = Request::singleton();
		if($clean) 
			return self::clean($req->params, $strip_tags);
		else 
			return $req->params;
	}

	public static function getStripTags($controller, $action)
	{
		$stripTags = App::config()->STRIP_TAGS;
		if(empty($stripTags))
			return '';

		foreach($stripTags as $allowed)
		{
			$allow = $allowed['allowed'];
			$tag_list = $allowed['tags'];
			$ctrl = strtolower($controller);
			
			if(isset($allow[$ctrl]) && is_array($allow[$ctrl]) && 
				in_array($action, $allow[$ctrl]))
			{
				return $tag_list;
			}
		}

		return '';
	}
	
	public static function clean(&$data, $strip_tags = '')
	{
		if($data==null) 
			$data = '';

		elseif(!is_array($data))
		{
			// where no http is found, add it - for wysihtml

			if(!empty($strip_tags))
				$data = self::targetUrls($data);   

			$data = strip_tags(trim($data), $strip_tags);
			
		}

		else
			foreach($data as $key=>$value)
				$data[$key] = self::clean($value, $strip_tags);
			
		return $data;
	}

    
    public static function getIP()
    {
        $ipaddress = '';

        if ($_SERVER['HTTP_CLIENT_IP'])
            $ipaddress = $_SERVER['HTTP_CLIENT_IP'];

        else if($_SERVER['HTTP_X_FORWARDED_FOR'])
            $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];

        else if($_SERVER['HTTP_X_FORWARDED'])
            $ipaddress = $_SERVER['HTTP_X_FORWARDED'];

        else if($_SERVER['HTTP_FORWARDED_FOR'])
            $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];

        else if($_SERVER['HTTP_FORWARDED'])
            $ipaddress = $_SERVER['HTTP_FORWARDED'];

        else if($_SERVER['REMOTE_ADDR'])
            $ipaddress = $_SERVER['REMOTE_ADDR'];

        else
            $ipaddress = 'UNKNOWN';

        return $ipaddress;
    }


	public static function targetUrls($item)
	{
		$item = preg_replace('/href="(?!http)(\S+)"/', 'href="http://$1"', $item);
		return preg_replace('/(<a[^>]+)>/', '$1 target="_blank">', $item); 
	}

	public static function createRedir($path)
	{
		if(!empty($path))
		{
			Request::singleton()->redir = $path; 
		}
	}

	public static function isMobile($checkTablet = false)
	{
		$mobile = new Mobile_Detect();

		if($checkTablet == false)
			return $mobile->isMobile();

		else
			return $mobile->isMobile() && $mobile->isTablet();
	}

	public function setCurrentUser($user_obj)
	{
		$user_obj->password = false;
		$this->curr_user = $user_obj;
	}
}