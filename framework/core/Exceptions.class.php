<?php

class RequestException extends Exception{}

class RedirException extends Exception{}

class ConfigException extends Exception{}

class AuthException extends Exception{}

class JSONException extends Exception {}

class ImageException extends Exception {}

class ErrException extends ErrorException 
{
	public function getError()
	{
		$e = $this; 

        $errfile = str_replace(
            App::config()->APP_PATH, "", $this->getFile()
        );

		return 
			'In file: '.$errfile. 
			' on line '.$this->getLine().
			': '. $this->getMessage();
	}
}

class TplException extends ErrException {}
