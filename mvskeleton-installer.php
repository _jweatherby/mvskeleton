<?php

$appDir = isset($argv[1]) ? $argv[1] : "app";

if(is_dir("./$appDir"))
	die("\nError: That directory already exists\n\n");

$thisDir = __DIR__;

shell_exec("cp -rp ".__DIR__.DIRECTORY_SEPARATOR."app $appDir");

if(!is_file(".gitignore"))
	shell_exec("cp {$thisDir}/.gitignore .");