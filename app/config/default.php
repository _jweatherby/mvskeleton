<?php

/* 

	These are the default environment settings. 
	It is advised to put the DEV environment settings in this file, then 
	override these settings on staging and production environments.
	To override these settings, create additional files:

		env.php
		keys.php

	These will not be included in gitignore, therefore can be used to 
	configure the different environments.

	The format for these files is simply to return an array as shown:

		i.e.
			<?php

			return array(
				var_name => array(settings), 
				var_name => value, 
				etc => etc
			);

	------------------------------------------------------------------------

	Additionally, if you wish to add sections (i.e. MAIN, CMS, CRM), etc.
	create files within config/ of the same name in lower case:

		i.e. 
			MAIN => main.php
			CMS => cms.php

	These will be included with the section being called.

	... and place additional settings within, including modified 

		CTRL_PATH
		VIEW_PATH
		MOBILE_PATH

	... pointing them to the correct root directories.
)


*/

error_reporting(E_ALL);

return array(

	'APP_PATH' => dirname(__DIR__).DIRECTORY_SEPARATOR,

	// env vars
	'ENV' => 'LOCAL',

	'DEBUG' => true,

	'PHP' => array(
		'display_errors' => 1,
		'log_errors' => 'on',
		'date.timezone' => 'Europe/London',
		'default_charset' => 'utf-8'
	),

	// controller/view directory - preceded by APP_PATH
	
	'CTRL_PATH' 	=> "mvc/Controllers/",
	'VIEW_PATH' 	=> "mvc/Views/",
	'PUBLIC_DIR' 	=> "web/",
	
	'ERROR_PATH' 	=> "mvc/Views/Errors/",

	// if composer.phar has been installed and Mobile_Detect has been found, 
	//	the MOBILE_PATH will be set. Otherwise, the VIEW_PATH will be used 
	// 	by default
	
	'MOBILE_PATH' 	=> "mvc/Views/", 	// - preceded by APP_PATH

	'DEFAULT_CONTROLLER' => 'Test',

	'ROUTER_SHORTCUTS' => array(

		':num' 		=> "([0-9]+)",
	 	':char' 	=> "([a-zA-Z]+)",
	 	':alpha' 	=> "([a-zA-Z0-9\-\_]+)"

	),

	'ROUTER' => array(
			// default : controller/id/action - necessary for error handling

		"([a-zA-Z]*)/?(\d*)/?([a-zA-Z\-_]*)/?" => 
			"{controller:$1}/{id:$2}/{action:$3}"
	),

	'VIEW_TYPES' => array('json','xml','rss','html','csv'),

	// the characters allowed in slugs
	'SLUG_CHARS' => "/[a-zA-Z0-9_\-]+/",

	// strip tags
	'STRIP_TAGS' => array(
		array(
			// controller = array(action) 
			'tags' => '<a><li><ol><ul><p><br><b><i><strong><span>',
			'allowed' => array(
				// controller => action
			)
		)
	),

	// default for pagination
	'PAGINATION' => array(
		
        'batch' => 20,
        'start' => 0,
        'sort'  => 'id',
        'dir'   => 'ASC',
        'type'  => false
    ),

	'JSON' => array( 'path' => "_json/" ),	// - preceded by APP_PATH

	'DB' => array(
		'HOST' 		=> 'localhost',
		'DATABASE' 	=> 'mvskeleton',
		'USERNAME' 	=> 'root',
		'PASSWORD'	=> ''
	),

	'AWS' => array(
		'KEY' 						=> "",
		'SECRET' 					=> "",
		"BUCKET" 					=> "",
		"ROOT_DIR" 					=> "",
		"FOLDER" 					=> ""
	),

	'TWITTER' => array(
	    'oauth_access_token' 		=> "",
	    'oauth_access_token_secret' => "",
	    'consumer_key' 				=> "",
	    'consumer_secret' 			=> ""
    ),

    'TPL_INHERITANCE' => array(
    	'BASE_TPL' => 'base.php'
    )
);