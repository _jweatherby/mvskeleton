<?php

return array(
	
	// controller/view directory

	// from the 'route' param
	'ROUTER' => array(

		// default : controller/id/action - necessary for error handling
		
		"([a-zA-Z]*)/?(\d*)/?([a-zA-Z]*)" => 
			"{controller:$1}/{id:$2}/{action:$3}"
	),

	'VIEW_TYPES' => array('json','xml','rss','html','csv'),

	// default controller if none specified
	'DEFAULT_CONTROLLER' => 'Test',

	// strip tags
	'STRIP_TAGS' => array(
		array(
			// controller = array(action) 
			'tags' => '<a><li><ol><ul><p><br><b><i><strong><span>',
			'allowed' => array(
				// controller => action
			)
		)
	),	
);