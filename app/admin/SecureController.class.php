<?php

abstract class SecureController extends BaseController
{
	protected $_admin;
	public function __construct()
	{
		//Session::start();
		if(class_exists('Session'))
			Session::start();
		else
			session_start(); 

		if(($admin = Admin::validate()) == false)
			throw new AuthException("No authenticated user found", 401);
		

		$this->_admin = $admin;
	}
}