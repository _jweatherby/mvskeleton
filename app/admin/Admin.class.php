<?php

class Admin extends BaseModel
{
	public 
		$id, $username, $password;

	protected 
		$_table = 'admin',
		$_db_vars = array('id', 'username', 'password');

	
	public static function validate()
	{
		if(!(isset($_SESSION['user_id']) && isset($_SESSION['username'])))
			return false; 

		$admin = new Admin(array(
			'id' 		=> $_SESSION['user_id'], 
			'username' 	=> $_SESSION['username']
		));
		
		return !empty($admin->id) && self::verifyIP() ? $admin : false;
	}

	public function verify($password)
	{
		$hash = new PasswordHash(8, true);
		return $hash->CheckPassword($password, $this->password);
	}

	public static function encrypt($password)
	{
		$hash = new PasswordHash(8, true);
		return $hash->HashPassword($password);
	}

	public function bindSession()
	{
		session_start();
		$_SESSION['user_id'] = $this->id; 
		$_SESSION['username'] = $this->username; 
		session_write_close();
	}

	public static function logout()
	{
		session_unset();
	}

	public static function verifyIP()
	{
		$ips = App::config()->IPs;

		$valid_ip = false;

		if(empty($ips)) return true;
		
		$this_ip = Request::getIP();
		foreach($ips as $ip) 
		{
			if($this_ip == $ip)
				return true;
		}

		return false;
	}
}