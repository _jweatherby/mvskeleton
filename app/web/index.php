<?php

$root = dirname(dirname(__DIR__));

// load the composer autoloader to get easy access to packages
require_once  $root . '/vendor/autoload.php';

// if installed at root
if(strpos(getcwd(), "vendor") === false) 
	require_once $root . "/App.php"; 

// if loaded through composer
else 
    require_once $root . "/vendor/dukeofweatherby/mvskeleton/App.php";

App::init(
	$app_path 	= dirname(__DIR__), 
	$app_type 	= 'nosqlapp',
	$section 	= 'MAIN'
);

App::htmlRequest($_REQUEST);