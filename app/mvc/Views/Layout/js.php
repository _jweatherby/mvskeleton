
	
	<!--script>
		var _gaq=[['_setAccount','UA-35078625-1'],['_trackPageview']];
		(function(d,t){var g=d.createElement(t),s=d.getElementsByTagName(t)[0];
		g.src=('https:'==location.protocol?'//ssl':'//www')+'.google-analytics.com/ga.js';
		s.parentNode.insertBefore(g,s)}(document,'script'));
	</script-->
	
	<script src="/vendor/jquery/jquery.min.js"></script>
	<script src="/vendor/bootstrap/dist/js/bootstrap.min.js"></script>
	<script src="/vendor/handlebars/handlebars.min.js"></script>
	<script src="/vendor/ember/ember.js"></script>
	<!--script src="/vendor/ember-addons.bs_for_ember/dist/js/bs-core.min.js"></script>
	<script src="/vendor/ember-addons.bs_for_ember/dist/js/bs-alert.min.js"></script-->
	<script src="/vendor/ember-data/ember-data.min.js"></script>
	<script src="/vendor/ckeditor/ckeditor.js"></script>
	<script src="/js/app.js"></script>
	<script>
		var _req = <?= json_encode($_req); ?>;
	</script>