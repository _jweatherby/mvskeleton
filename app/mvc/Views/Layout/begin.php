<!doctype html>
<!--[if lt IE 7]> <html class="no-js ie6 ltie9 ie" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js ie7 ltie9 ie" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js ie8 ltie9 ie" lang="en"> <![endif]-->
<!--[if IE 9]>    <html class="no-js ie9 ie" lang="en"> <![endif]-->
<!--[if gt IE 9]><!--> <html class="no-js no-oldie noie10" lang="en"> <!--<![endif]-->
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<title>Hello World!</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <meta name="description" content="A barebones application"> 
	<link rel="stylesheet" href="/vendor/bootstrap/dist/css/bootstrap.css">
	<!--script src="/js/kc-functions.js?v=2.7"></script-->
</head>
<body class="noweird-chrome no-scrolling">
	<div class="contain-all">
		
			