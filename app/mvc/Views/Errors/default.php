<div class="main-content">

	<h2>
		<?= "Uh Oh, Spaghetti-Os!"; ?>
		<?php 
			if($_req->err_type && App::isDebugEnabled()) 
				echo "- ".$_req->err_type; 
		?>
	</h2>
	<p></p>
	<h4><?= (!empty($e) ? $e->getMessage() : "Generic Error Message"); ?></h4>


	<?php if(App::isDebugEnabled()){ ?>

	<div class="vert-padding vert-margin">
	<h3>You appear to not be on LIVE, so here's some more info: </h3>

	<div class="side-margin">
		<?php  if(!empty($e)){ ?> 
		<p>
			<strong>Code:</strong>
			<?= $e->getCode(); ?>
		</p>

		<p>
			<strong>Message:</strong> 
			<?= $e->getMessage(); ?>
		</p>
		<?php } ?>
		<p>
			<strong>Backtrace:</strong>
			<pre><?php debug_print_backtrace(); ?></pre>
		</p>
	</div>
	</div>
	<?php } ?>

</div>