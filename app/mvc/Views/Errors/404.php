<div id="content" role="main" class="cs2">
	<div class="wrap">
		<article class="content-article">
			<header>
				<h2 class="title">
					404 - Page not found.
				</h2>
				<h3 class="subtitle">
					<p>Try using the main navigation at the top of the page, or return to the <a href="#contact" class="theme-font">home page</a>. </p>
					<p>You can also <a href="#email" class="theme-font">contact us</a> for further assistance.</p>
				</h3>
				<p class="header-text p404 ir">
					404
				</p>
			</header>
			<div class="wrap"></div>
		</article>
	</div>
</div>