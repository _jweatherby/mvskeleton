<?php 

$e = Request::getException();
$err_type = $_req->err_type; 

if($e->getCode() == 404 || $e->getCode() == 410) {
    Tpl::getError("404", array( 'e' => $e )); 
}

else if($e->getCode() == 401) {
	Tpl::getError("401", array( 'e' => $e )); 
}

else if($err_type == 'Err') {
	Tpl::getError("error", array( 'e' => $e )); 
}

else if($err_type == 'Tpl') {
	Tpl::getError("tplerror", array( 'e' => $e )); 
}

else {
	Tpl::getError("error");
}