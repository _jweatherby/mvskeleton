<form action="/cms/admin/login" method="POST">
	<h2>Please Login Here.</h2>
	<p><?= $e->getMessage(); ?>
	<div class="padded">
		<strong>Username</strong> 
		<input type="text" name="username" placeholder="Username">
	</div>
	<div class="padded">
		<strong>Password&nbsp;</strong> 
		<input type="password" name="password" placeholder="Password">
	</div>
	<div class="overflow">
		<input type="submit" class="button" value="Login">
	</div>
</form>